package com.avantaj.bhagavadgita;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.avantaj.bhagavadgita.db.CacheDB;
import com.avantaj.bhagavadgita.model.Shologam;
import com.avantaj.bhagavadgita.service.BackgroundSoundService;

public class BhagavadhGitaActivity extends FragmentActivity implements OnTabChangeListener{
    /** Called when the activity is first created. */
	public static final String  RANDOM_HOME ="Random";
	public static final String  CHAPTER_HOME ="Chapter";
	public static final String  FAVORITE_HOME ="Favorite";
	public static  boolean  soundBtn=false;
	FragmentManager fm;
	private RandomFragment randomFragment = new RandomFragment();
	TabHost tabHost;
	private JSONObject jObj;
	private JSONArray jArray;
	public static  ArrayList<Shologam> shologams= new ArrayList<Shologam>();
	private ArrayList<Shologam> tempShologams= new ArrayList<Shologam>();
	public static Intent svc;
	//private boolean checkDB;
	
	private int mCurrentTab;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        init();
    }
    private void initialization(){
		 if (fm.findFragmentByTag(RANDOM_HOME) == null) {
	            fm.beginTransaction()
	                    .replace(android.R.id.tabcontent,randomFragment, RANDOM_HOME)
	                    .commitAllowingStateLoss();
	        }
	}
	public void onTabChanged(String tabId) {
		if(RANDOM_HOME.equals(tabId)){
			if(fm.getBackStackEntryCount() > 0 ){
				fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				fm.executePendingTransactions();
			}
			if (fm.findFragmentByTag(tabId) == null) {
	            fm.beginTransaction()
	                    .replace(android.R.id.tabcontent, new RandomFragment(), tabId)
	                    .commit();
	        }
            mCurrentTab = 0;
            return;
	}
	if(CHAPTER_HOME.equals(tabId)){
			if(fm.getBackStackEntryCount() > 0 ){
				fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				fm.executePendingTransactions();
			}
			if (fm.findFragmentByTag(tabId) == null) {
	            fm.beginTransaction()
	                    .replace(android.R.id.tabcontent, new ChapterListFragment(), tabId)
	                    .commit();
	        }
            mCurrentTab = 1;
            return;
	}
	if(FAVORITE_HOME.equals(tabId)){
		
		
		if(fm.getBackStackEntryCount() > 0 ){
			fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			fm.executePendingTransactions();
		}
		if (fm.findFragmentByTag(tabId) == null) {
            fm.beginTransaction()
                    .replace(android.R.id.tabcontent, new FavoriteListFragment(), tabId)
                    .commit();
        }
        mCurrentTab = 2;
        return;
	}
}
	@Override
	protected void onPause() {
		super.onPause();
		stopService(svc);
	}
	@Override
	protected void onStop() {
		super.onStop();
		stopService(svc);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopService(svc);
	}
	private View createTab(String title, int resourceId)
	{
		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.tab_indicator, tabHost.getTabWidget() , false);
		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/Rachana_04.ttf");
		((TextView)(view.findViewById(R.id.title))).setTypeface(tf);
		((TextView)(view.findViewById(R.id.title))).setText(title);
		((ImageView)(view.findViewById(R.id.icon))).setBackgroundResource(resourceId);
		return view;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(BhagavadhGitaActivity.soundBtn){
			startService(svc);
		}
	}
	private void insertInDB(ArrayList<Shologam> shologams){
		CacheDB cb = new CacheDB(this);
		cb.open();
		ContentValues insert = new ContentValues();
		for (int i = 0; i < shologams.size(); i++) {
			insert.put("_id", shologams.get(i).getId());
			insert.put("prime_id", shologams.get(i).getQuote_id());
			insert.put("chap_id", shologams.get(i).getChap_id());
			insert.put("upavacha_id", shologams.get(i).getUpavcha());
			insert.put("sam_text", shologams.get(i).getSam_text());
			insert.put("mal_text",shologams.get(i).getMal_text());
			insert.put("fav", String.valueOf(shologams.get(i).isFavorite()));
			 cb.insert(insert);
		}
		 cb.close();
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {       
	    super.onConfigurationChanged(newConfig);
	    init();
	}
	private void init(){
		svc=new Intent(this, BackgroundSoundService.class);
		if(BhagavadhGitaActivity.soundBtn){
			startService(svc);
		}
        File database = getDatabasePath("cache.db");
        if(!database.exists()){
        	startService(svc);
        	runOnUiThread(new Runnable(){
				public void run() {
						new AsyncCaller().execute();
				}
        	});
        }
        else{
        	CacheDB cb = new CacheDB(this);
        	cb.open();
        	shologams = cb.getFavoriteList();
        	if(shologams.size()==0){
        		runOnUiThread(new Runnable(){
    				public void run() {
    						new AsyncCaller().execute();
    				}
            	});
        		cb.close();
        		return;
        	}
        	cb.close();
        	startScreen();
        }
	}
	private class AsyncCaller extends AsyncTask<Void, Void, Void>
	{
	    ProgressDialog pdLoading = new ProgressDialog(BhagavadhGitaActivity.this);
	    
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        //this method will be running on UI thread
	        pdLoading.setMessage("");
	        pdLoading.show();
	        pdLoading.setCancelable(false);
	        pdLoading.setContentView(R.layout.custom_progressbar);
	    }
	    @Override
	    protected Void doInBackground(Void... params) {
	    	
	    	System.out.println("I am in G");
			try {
				jObj = new JSONObject(jsonToStringFromAssetFolder("BhagvadGita.json",BhagavadhGitaActivity.this));
				jArray = jObj.getJSONArray("Gita");
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject shologam = jArray.getJSONObject(i);
					String prim_id = shologam.getString("prim_id");
					String chapter = shologam.getString("chapter");
					String  quotesID = shologam.getString("quotesID");
					String userID = shologam.getString("userID");
					String quotes_sanskrit = shologam.getString("quotes_sanskrit");
					String quotes_mal = shologam.getString("quotes_mal");
					tempShologams.add(new Shologam(Integer.parseInt(prim_id), quotesID, Integer.parseInt(chapter),Integer.parseInt(userID), quotes_sanskrit, quotes_mal,false));
				}
				insertInDB(tempShologams);
			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

	        return null;
	    }
	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);

	        //this method will be running on UI thread
	      CacheDB cb = new CacheDB(BhagavadhGitaActivity.this);
        	cb.open();
        	shologams = cb.getFavoriteList();
        	System.out.println(shologams.size());
        	cb.close();
        	pdLoading.dismiss();
        	startScreen();
	    }
	    public String jsonToStringFromAssetFolder(String fileName,Context context) throws IOException {
	        AssetManager manager = context.getAssets();
	        InputStream file = manager.open(fileName);

	        byte[] data = new byte[file.available()];
	        file.read(data);
	        file.close();
	        return new String(data);
	    }
	 }
	private void startScreen()
	{
		 fm= getSupportFragmentManager();
	        tabHost =  (TabHost) findViewById(android.R.id.tabhost);
	        tabHost.setup();
	        TabSpec randomSpec = tabHost.newTabSpec("Random");
	        randomSpec.setIndicator(createTab("ശ്ലോകങ്ങൾ",R.drawable.random_btn_tab));
	        randomSpec.setContent(android.R.id.tabcontent);
	        TabSpec chapterSpec = tabHost.newTabSpec("Chapter");
	        chapterSpec.setIndicator(createTab("അധ്യായങ്ങൾ", R.drawable.chapter_btn));
	        chapterSpec.setContent(android.R.id.tabcontent);
	        TabSpec favoriteSpec = tabHost.newTabSpec("Favorite");
	        favoriteSpec.setIndicator(createTab("പ്രിയപ്പെട്ടവ", R.drawable.star_btn_tab));
	        favoriteSpec.setContent(android.R.id.tabcontent);
	        tabHost.addTab(randomSpec); 
	        tabHost.addTab(chapterSpec);
	        tabHost.addTab(favoriteSpec);
	        tabHost.setOnTabChangedListener(this);
	        tabHost.setCurrentTab(mCurrentTab);
	        initialization();
	}
}
