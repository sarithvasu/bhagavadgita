package com.avantaj.bhagavadgita;



import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avantaj.bhagavadgita.adapter.MyPagerAdapter;
import com.avantaj.bhagavadgita.model.Shologam;

@SuppressLint("ValidFragment")
public class ChapterFragment extends Fragment {
	
	MediaPlayer player;
	ViewPager viewPager;
	ImageView leftBtn,rightBtn,soundBtn;
	ArrayList<Shologam> shologams ;
	Intent svc;
	public ChapterFragment(){
		
	}
	@SuppressLint("ValidFragment")
	public ChapterFragment(ArrayList<Shologam> shologams) {
		this.shologams = shologams;
	}
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View chapterView = inflater.inflate(R.layout.chapter_layout, container,false);
		viewPager =(ViewPager)chapterView. findViewById(R.id.viewpager);
		MyPagerAdapter adapter = new MyPagerAdapter(getActivity(),BhagavadhGitaActivity.shologams,false,false);
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(shologams.get(0).getId()-1);
		return chapterView;
	}
    @Override
    public void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	viewPager.getAdapter().notifyDataSetChanged();
    }
}
