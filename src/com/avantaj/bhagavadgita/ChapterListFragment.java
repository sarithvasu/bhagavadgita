package com.avantaj.bhagavadgita;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avantaj.bhagavadgita.adapter.ChapterListAdapter;
import com.avantaj.bhagavadgita.model.Shologam;
import com.avantaj.bhagavadgita.service.BackgroundSoundService;

@SuppressLint("ValidFragment")
public class ChapterListFragment extends Fragment{
	ChapterListAdapter chapterListAdapter;
	ArrayList<Shologam> shologams;
	ImageView soundBtn;
	TextView heading;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	 // TODO Auto-generated method stub
	 super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.chapter_list, container, false);
    	heading = (TextView)view.findViewById(R.id.heading_list);
    	 Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Rachana_04.ttf");
    	 heading.setTypeface(tf);
    	 heading.setTypeface(tf);
    	heading.setText("അധ്യായങ്ങൾ ");
    	soundBtn = (ImageView)view.findViewById(R.id.sound_btn_in_list);
		if(isMyServiceRunning()){
			soundBtn.setImageResource(R.drawable.sound_on);
			BhagavadhGitaActivity.soundBtn = true;
		}
		else{
			soundBtn.setImageResource(R.drawable.soud_off);
			BhagavadhGitaActivity.soundBtn = false;
		}
			
		soundBtn.setTag("hjdj");
		soundBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if(isMyServiceRunning()){
					soundBtn.setImageResource(R.drawable.soud_off);
					BhagavadhGitaActivity.soundBtn = false;
					getActivity().stopService(BhagavadhGitaActivity.svc);
				
				}
				else{
					soundBtn.setImageResource(R.drawable.sound_on);
					BhagavadhGitaActivity.soundBtn = true;
					getActivity().startService(BhagavadhGitaActivity.svc);
				}
			}
		});
    	String[] adhyayam ={"അർജുന വിഷാദയോഗഃ", "സാംഖ്യായോഗഃ", "കർമയോഗഃ", "ജ്ഞാനകർമസംന്യാസയോഗഃ", "സംന്യാസയോഗഃ", "ആത്മസംയമയോഗഃ", "ജ്ഞാനവിജ്ഞാനയോഗഃ", "അക്ഷരബ്രഹ്മയോഗഃ", "രാജവിദ്യാരാജഗുഹ്യയോഗഃ", "വിഭൂതിയോഗഃ", "വിശ്വരൂപദർശനയോഗഃ", "ഭക്തിയോഗഃ", "ക്ഷേത്രക്ഷേത്രജ്ഞവിഭാഗയോഗഃ", "ഗുണത്രയവിഭാഗയോഗഃ", "പുരുഷോത്തമയോഗഃ", "ദൈവാസുരസമ്പദ്വിഭാഗയോഗഃ", "ശ്രദ്ധാത്രയവിഭാഗയോഗഃ", "മോക്ഷസംന്യാസയോഗഃ"};
		  ListView lv = (ListView)view.findViewById(R.id.list);
		  if(getActivity()!=null)
			chapterListAdapter = new ChapterListAdapter(getActivity(), R.layout.chapter_list_item,adhyayam, BhagavadhGitaActivity.shologams);
		  lv.setAdapter(chapterListAdapter);
   	
    	return view;
    }
	private boolean isMyServiceRunning() {
		if(getActivity()!=null){
			ActivityManager manager = (ActivityManager)getActivity(). getSystemService(Context.ACTIVITY_SERVICE);
			for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
				if (BackgroundSoundService.class.getName().equals(service.service.getClassName())) {
					return true;
				}
			}
		}        
		return false;
    }
}
