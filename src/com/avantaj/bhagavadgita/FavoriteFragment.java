package com.avantaj.bhagavadgita;



import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avantaj.bhagavadgita.adapter.MyPagerAdapter;
import com.avantaj.bhagavadgita.model.Shologam;

@SuppressLint("ValidFragment")
public class FavoriteFragment extends Fragment {
	TextView malText;
	ViewPager viewPager;
	ImageView leftBtn,rightBtn,soundBtn;
	ArrayList<Shologam> shologams = new ArrayList<Shologam>();
	Shologam shologam;
	public FavoriteFragment() {
		// TODO Auto-generated constructor stub
	}
	public FavoriteFragment(Shologam shologam) {
		this.shologam = shologam;
	}
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
View favoriteView = inflater.inflate(R.layout.favorite_layout, container,false);
		
		
		
		viewPager =(ViewPager)favoriteView. findViewById(R.id.viewpager);
		viewPager.setCurrentItem(0);
		shologams.add(shologam);
		MyPagerAdapter adapter = new MyPagerAdapter(getActivity(),shologams,true,false);
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(0);
		
		return favoriteView;
    }
    /*private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager)getActivity(). getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (BackgroundSoundService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }*/
}