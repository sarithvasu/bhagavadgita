package com.avantaj.bhagavadgita;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avantaj.bhagavadgita.adapter.FavoriteListAdapter;
import com.avantaj.bhagavadgita.db.CacheDB;
import com.avantaj.bhagavadgita.model.Shologam;
import com.avantaj.bhagavadgita.service.BackgroundSoundService;

@SuppressLint("ValidFragment")
public class FavoriteListFragment extends Fragment {
	FavoriteListAdapter chapterListAdapter;
	ArrayList<Shologam> favShologams;
	ImageView soundBtn;
	TextView heading;
	Typeface tf;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	 super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
    		View view = inflater.inflate(R.layout.favorite_list, container, false);
    		heading = (TextView)view.findViewById(R.id.heading_list);
    		if(getActivity()!=null){
    				tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Rachana_04.ttf");
       	 			heading.setTypeface(tf);
       	 			heading.setText("പ്രിയപ്പെട്ടവ ");
       	 	}
    		ListView lv = (ListView)view.findViewById(R.id.fav_list);
    		soundBtn = (ImageView)view.findViewById(R.id.sound_btn_in_list);
    		if(isMyServiceRunning()){
    			soundBtn.setImageResource(R.drawable.sound_on);
    			BhagavadhGitaActivity.soundBtn = true;
    		}
    		else{
    			soundBtn.setImageResource(R.drawable.soud_off);
    			BhagavadhGitaActivity.soundBtn = false;
    		}
    			
    		soundBtn.setTag("hjdj");
    		soundBtn.setOnClickListener(new OnClickListener() {
    			
    			public void onClick(View v) {
    				if(isMyServiceRunning()){
    					soundBtn.setImageResource(R.drawable.soud_off);
    					BhagavadhGitaActivity.soundBtn = false;
    					getActivity().stopService(BhagavadhGitaActivity.svc);
    				
    				}
    				else{
    					soundBtn.setImageResource(R.drawable.sound_on);
    					BhagavadhGitaActivity.soundBtn = true;
    					getActivity().startService(BhagavadhGitaActivity.svc);
    				}
    			}
    		});
    		if(getActivity()!=null){
    			CacheDB cb = new CacheDB(getActivity());
    			cb.open();
    				favShologams =  cb.getFavoriteList();
    			cb.close();
    			if(getActivity()!=null)
    				chapterListAdapter = new FavoriteListAdapter(getActivity(), R.layout.chapter_list_item,makeFavoite(favShologams));
    			lv.setAdapter(chapterListAdapter);
    		}
    		return view;
    }
	private ArrayList<Shologam> makeFavoite(ArrayList<Shologam> shologams){
		ArrayList<Shologam> favShologams = new ArrayList<Shologam>();
		for (int i = 0; i < shologams.size(); i++) {
			if(shologams.get(i).isFavorite()){
				favShologams.add(shologams.get(i));
			}
		}
		return favShologams;
	}
	private boolean isMyServiceRunning() {
		if(getActivity()!=null){
			ActivityManager manager = (ActivityManager)getActivity(). getSystemService(Context.ACTIVITY_SERVICE);
			for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
				if (BackgroundSoundService.class.getName().equals(service.service.getClassName())) {
					return true;
				}
			}
		}        
		return false;
    }
	@Override
	public void onConfigurationChanged(Configuration newConfig) {       
	    super.onConfigurationChanged(newConfig);
	    if(getActivity()!=null){
	    	tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Rachana_04.ttf");
   	 			heading.setTypeface(tf);
   	 			heading.setText("പ്രിയപ്പെട്ടവ ");
   	 	}
	}
}
