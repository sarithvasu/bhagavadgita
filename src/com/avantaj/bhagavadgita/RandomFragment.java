package com.avantaj.bhagavadgita;



import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avantaj.bhagavadgita.adapter.MyPagerAdapter;
import com.avantaj.bhagavadgita.model.Shologam;

@SuppressLint("ValidFragment")
public class RandomFragment extends Fragment  {
	TextView malText;
	ViewPager viewPager;
	ArrayList<Shologam> shologams;
	ImageView leftBtn,rightBtn,soundBtn;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        }
   
    @SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
    	View rondomView = inflater.inflate(R.layout.random_layout, container, false);
		viewPager =(ViewPager)rondomView. findViewById(R.id.viewpager);
		viewPager.setCurrentItem(0);
		shologams  = (ArrayList<Shologam>) BhagavadhGitaActivity.shologams.clone();
		shuffleList(shologams);
		MyPagerAdapter adapter = new MyPagerAdapter(getActivity(),shologams,false,true);
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(0);
		viewPager.getAdapter().notifyDataSetChanged();
    	return rondomView;
    }
    public static void shuffleList(ArrayList<Shologam> shologams) {
        int n = shologams.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
          int change = i + random.nextInt(n - i);
          swap(shologams, i, change);
        }
      }
      private static void swap(ArrayList<Shologam> a, int i, int change) {
        Shologam helper = a.get(i);
        a.set(i, a.get(change));
        a.set(change, helper);
      }
	@Override
    public void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	if(viewPager!=null)
    		viewPager.getAdapter().notifyDataSetChanged();
    }
}