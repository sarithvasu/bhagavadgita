package com.avantaj.bhagavadgita;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Splash extends Activity {
	ImageView slide_lay;
	private Handler handler = new Handler();

	/** Called when the activity is first created. */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		slide_lay = (ImageView) findViewById(R.id.splash_img);
		Animation hyperspaceJump = AnimationUtils.loadAnimation(this, R.anim.scaleme);
		slide_lay.startAnimation(hyperspaceJump);
		initialize();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			Log.e("Splash", "BACK KEY CONSUMED");
			return true;
		}
		return true;
	}
	// To disable home buttons
	@Override
	public void onAttachedToWindow() {
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		super.onAttachedToWindow();
	}
	// Setting the timer to run after 3 secs.
	private void initialize() {
		handler.postDelayed(new Runnable() {
			public void run() {
				// Calling the next Activity.
				Intent intent = new Intent(Splash.this, BhagavadhGitaActivity.class);
				startActivity(intent);
				finish();
			}

		}, 3000);
	}
}
