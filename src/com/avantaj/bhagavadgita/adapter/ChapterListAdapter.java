package com.avantaj.bhagavadgita.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.avantaj.bhagavadgita.ChapterFragment;
import com.avantaj.bhagavadgita.R;
import com.avantaj.bhagavadgita.model.Shologam;

public class ChapterListAdapter extends ArrayAdapter<String>  {
	Context context;
	ArrayList<Shologam> shologams;
	ArrayList<Shologam> chapterShologams;
	String[]  adhyayam;
	TextView title;
	FragmentManager fm;
	
	public static final String CHAPTER_LIST_CONTENT = "chapter_list_content";
	public ChapterListAdapter(FragmentActivity context, int textViewResourceId,String[] adhyayam,ArrayList<Shologam> shologams) {
		super(context,textViewResourceId,adhyayam);
		this.context = context;
		this.adhyayam = adhyayam;
		this.shologams = shologams;
		fm = context.getSupportFragmentManager();
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = convertView;
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.chapter_list_item, null);
		}
		Typeface tf = Typeface.createFromAsset(context.getAssets(),"fonts/Rachana_04.ttf");
		title = (TextView)row.findViewById(R.id.chapter_title);
		title.setTypeface(tf);
		title.setText(position+1+" : "+adhyayam[position]);
		row.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				chapterShologams = new ArrayList<Shologam>();
				for (int i = 0; i < shologams.size(); i++) {
					if(shologams.get(i).getChap_id()==(position+1)){
						chapterShologams.add(shologams.get(i));
					}
				}
				if ( fm.findFragmentByTag(CHAPTER_LIST_CONTENT) == null) {
		            fm.beginTransaction()
		                    .replace(android.R.id.tabcontent, new ChapterFragment(chapterShologams), CHAPTER_LIST_CONTENT)
		                    .addToBackStack(null)
		                    .commit();
		        
				}
				
			}
		});
		return row;
	}
}
