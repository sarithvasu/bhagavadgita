package com.avantaj.bhagavadgita.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.avantaj.bhagavadgita.FavoriteFragment;
import com.avantaj.bhagavadgita.R;
import com.avantaj.bhagavadgita.model.Shologam;

public class FavoriteListAdapter extends ArrayAdapter<Shologam> {
	Context context;
	ArrayList<Shologam> shologams;
	TextView title;
	FragmentManager fm;
	public static final String FAVORITE_LIST_CONTENT = "favorite_list_content";
	public FavoriteListAdapter(FragmentActivity context, int textViewResourceId,ArrayList<Shologam> shologams) {
		super(context,textViewResourceId,shologams);
		this.context = context;
		this.shologams = shologams;
		fm = context.getSupportFragmentManager();
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = convertView;
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.favorite_list_item, null);
			System.out.println("convertView null");
		}
		Typeface tf = Typeface.createFromAsset(context.getAssets(),"fonts/Rachana_04.ttf");
		title = (TextView)row.findViewById(R.id.favorite_title);
		title.setTypeface(tf);
		title.setText(shologams.get(position).getSam_text());
		row.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if ( fm.findFragmentByTag(FAVORITE_LIST_CONTENT) == null) {
		            fm.beginTransaction()
		                    .replace(android.R.id.tabcontent, new FavoriteFragment(shologams.get(position)), FAVORITE_LIST_CONTENT)
		                    .addToBackStack(null)
		                    .commit();
		        
				}
				
			}
		});
		return row;
	}
}
