package com.avantaj.bhagavadgita.adapter;

import java.util.ArrayList;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avantaj.bhagavadgita.BhagavadhGitaActivity;
import com.avantaj.bhagavadgita.R;
import com.avantaj.bhagavadgita.db.CacheDB;
import com.avantaj.bhagavadgita.model.Shologam;
import com.avantaj.bhagavadgita.service.BackgroundSoundService;

public class MyPagerAdapter extends PagerAdapter {
	
		Context context;
		TextView tv1,tv2,heading,upavachaTv;
		boolean fav,random;
		Shologam favCheckShologam;
		ImageView leftBtn,rightBtn,soundBtn,starBtn,randomBtn;
		String[] upavacha= {"ധൃതരാഷ്ട്ര ഉവാച:", "സഞ്ജയ ഉവാച:", "അർജുന ഉവാച:", "ശ്രീഭഗവാനുവാച:"};
		String[] adhyayam = {"അർജുന വിഷാദയോഗഃ", "സാംഖ്യായോഗഃ", "കർമയോഗഃ", "ജ്ഞാനകർമസംന്യാസയോഗഃ", "സംന്യാസയോഗഃ", "ആത്മസംയമയോഗഃ", "ജ്ഞാനവിജ്ഞാനയോഗഃ", "അക്ഷരബ്രഹ്മയോഗഃ", "രാജവിദ്യാരാജഗുഹ്യയോഗഃ", "വിഭൂതിയോഗഃ", "വിശ്വരൂപദർശനയോഗഃ", "ഭക്തിയോഗഃ", "ക്ഷേത്രക്ഷേത്രജ്ഞവിഭാഗയോഗഃ", "ഗുണത്രയവിഭാഗയോഗഃ", "പുരുഷോത്തമയോഗഃ", "ദൈവാസുരസമ്പദ്വിഭാഗയോഗഃ", "ശ്രദ്ധാത്രയവിഭാഗയോഗഃ", "മോക്ഷസംന്യാസയോഗഃ"};
		ArrayList<Shologam> Shologams;
		FragmentManager fm;
	public MyPagerAdapter(Context context, ArrayList<Shologam> Shologams,boolean fav ,boolean random) {
		super();
		this.context = context;
		this.Shologams = Shologams;
		this.fav = fav;
		this.random=random;
	}
	
	@Override
	public int getCount() {
	    // TODO Auto-generated method stub
	    return Shologams.size();
	}
	public Object instantiateItem(final ViewGroup collection, final int pos){
	    LayoutInflater inflater =     (LayoutInflater)collection.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    final Shologam shologam = Shologams.get(pos);
	    final View view = inflater.inflate(R.layout.viewpage, null);
	    tv1=(TextView) view.findViewById(R.id.tv1);
	    tv2=(TextView) view.findViewById(R.id.tv2);
	    heading = (TextView)view.findViewById(R.id.heading);
	    upavachaTv = (TextView)view.findViewById(R.id.tv3);
	    starBtn = (ImageView)view.findViewById(R.id.star_btn);
	    CacheDB db = new CacheDB(context);
		db.open();
		favCheckShologam =db.getFavById(shologam.getId());
		db.close();
	    if(favCheckShologam.isFavorite()){
	    	starBtn.setImageResource(R.drawable.star_active);
	    }
	    else{
	    	starBtn.setImageResource(R.drawable.star_btn);
	    }
	    starBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				 CacheDB db = new CacheDB(context);
					db.open();
					favCheckShologam =db.getFavById(shologam.getId());
					db.close();
				if(!favCheckShologam.isFavorite()){
					ContentValues updateFav = new ContentValues();
					updateFav.put("_id", shologam.getId());
					updateFav.put("prime_id", shologam.getQuote_id());
					updateFav.put("chap_id", shologam.getChap_id());
					updateFav.put("upavacha_id", shologam.getUpavcha());
					updateFav.put("sam_text", shologam.getSam_text());
					updateFav.put("mal_text",shologam.getMal_text());
					updateFav.put("fav", "true");
					CacheDB db1 = new CacheDB(context);
					db1.open();
					if(db1.updateFav(updateFav, String.valueOf(shologam.getId()))){
						starBtn.setImageResource(R.drawable.star_active);
						notifyDataSetChanged();
					}
					db1.close();
				}
				else{
					ContentValues updateFav = new ContentValues();
					updateFav.put("_id", shologam.getId());
					updateFav.put("prime_id", shologam.getQuote_id());
					updateFav.put("chap_id", shologam.getChap_id());
					updateFav.put("upavacha_id", shologam.getUpavcha());
					updateFav.put("sam_text", shologam.getSam_text());
					updateFav.put("mal_text",shologam.getMal_text());
					updateFav.put("fav", "false");
					CacheDB db1 = new CacheDB(context);
					db1.open();
					if(db1.updateFav(updateFav, String.valueOf(shologam.getId()))){
						starBtn.setImageResource(R.drawable.star_btn);
						notifyDataSetChanged();
					}
					db1.close();
				}
	    		
			}
		});
	    Typeface tf = Typeface.createFromAsset(context.getAssets(),"fonts/Rachana_04.ttf");
	    tv1.setTypeface(tf);
	    tv2.setTypeface(tf);
	    upavachaTv.setTypeface(tf);
	    heading.setTypeface(tf);
	    tv1.setText(shologam.getSam_text());
	    tv2.setText(shologam.getMal_text());
	    heading.setText(shologam.getChap_id()+" : "+adhyayam[shologam.getChap_id()-1]);
	    upavachaTv.setText("ശ്ലോകം :"+shologam.getQuote_id()+"\n"+upavacha[shologam.getUpavcha()-1]);
	    leftBtn = (ImageView)view.findViewById(R.id.left_btn);
		rightBtn = (ImageView)view.findViewById(R.id.right_btn);
		randomBtn = (ImageView)view.findViewById(R.id.random_in_page);
		randomBtn.setVisibility(View.GONE);
		if(random){
			System.out.println("I AM RANDOM");
			leftBtn.setVisibility(View.GONE);
			rightBtn.setVisibility(View.GONE);
			randomBtn.setImageResource(R.drawable.random_btn);
			randomBtn.setVisibility(View.VISIBLE);
			
		}
		randomBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				((ViewPager)collection).setCurrentItem(pos+1,true);
			}
		});
		if(fav)
		{
			leftBtn.setVisibility(View.GONE);
			rightBtn.setVisibility(View.GONE);
		}
		if(pos==0)
			leftBtn.setVisibility(View.GONE);
		if(pos==Shologams.size()-1)
			rightBtn.setVisibility(View.GONE);
		leftBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				((ViewPager)collection).setCurrentItem(pos-1,true);
			}
		});
		rightBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				((ViewPager)collection).setCurrentItem(pos+1,true);
			}
		});
		soundBtn = (ImageView)view.findViewById(R.id.sound_btn);
		if(isMyServiceRunning()){
			soundBtn.setImageResource(R.drawable.sound_on);
			BhagavadhGitaActivity.soundBtn = true;
		}
		else{
			soundBtn.setImageResource(R.drawable.soud_off);
			BhagavadhGitaActivity.soundBtn = false;
		}
			
		soundBtn.setTag("hjdj");
		soundBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if(isMyServiceRunning()){
					soundBtn.setImageResource(R.drawable.soud_off);
					context.stopService(BhagavadhGitaActivity.svc);
					BhagavadhGitaActivity.soundBtn = false;
					notifyDataSetChanged();
				}
				else{
					soundBtn.setImageResource(R.drawable.sound_on);
					context.startService(BhagavadhGitaActivity.svc);
					BhagavadhGitaActivity.soundBtn = true;
					notifyDataSetChanged();
				}
			}
		});
	    ((ViewPager)collection).addView(view, 0);
	    return view;

	}
	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0 == ((View)arg1);
	}

	 @Override
	 public Parcelable saveState() {
	  return null;
	 }
	 private boolean isMyServiceRunning() {
	        ActivityManager manager = (ActivityManager)context. getSystemService(Context.ACTIVITY_SERVICE);
	        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	            if (BackgroundSoundService.class.getName().equals(service.service.getClassName())) {
	                return true;
	            }
	        }
	        return false;
    }

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub
		((ViewPager)container).removeView((View)object);
	}
	public int getItemPosition(Object object) {
	    return POSITION_NONE;
	}
}
