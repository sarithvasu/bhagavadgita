package com.avantaj.bhagavadgita.db;

import java.util.ArrayList;

import com.avantaj.bhagavadgita.model.Shologam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class CacheDB  {
	private static final String DATABASE_NAME = "cache.db";
	private static final String TABLE_NAME = "favoriteTable";
	//private static final String DATABASE_CREATE = "create table if not exists "+TABLE_NAME+" " + "(_id integer,prime_id integer,chap_id integer,upavacha_id integer,sam_text text,mal_text text,fav text);";
	private Context context;
	private toDoCacheDbHelper dbHelper;
	private SQLiteDatabase db;
	public CacheDB(Context context) {
		this.context = context;
		if(context!=null){
			
			dbHelper = new toDoCacheDbHelper(context, DATABASE_NAME, null, 3);
			System.out.println("created DB");
		}	
	}
	public void open() throws SQLiteException{

		try
		{
				db = dbHelper.getWritableDatabase();
			
		}catch(SQLiteException e)
		{
			e.printStackTrace();
			db = dbHelper.getReadableDatabase();
		}
	}
	public void close()
	{
		if(db!=null)
			db.close();
	}
	public void dropDatabase(){
		context.deleteDatabase(DATABASE_NAME);
	}
	public long insert(ContentValues contentValues)
	{
		if(db!=null)
			return db.insert(TABLE_NAME, null, contentValues);
		else
			return -1;
	}
	
	
	public ArrayList<Shologam> getFavoriteList(){
		open();
		ArrayList<Shologam> favShologams = new ArrayList<Shologam>();
		String[] result_column = new String[]{"_id" ,"prime_id","chap_id","upavacha_id","sam_text","mal_text","fav"};
		Cursor c =db.query(TABLE_NAME, result_column, null, null, null, null, null);
		
		if(c.moveToFirst())
			do{
				try {
					favShologams.add(new Shologam(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getString(4),c.getString(5) ,c.getString(6).equals("true")))  ; 
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}while(c.moveToNext());
		c.close();
		close();
		return favShologams;
	}
	public Shologam getFavById(int id){
		String where = "_id="+id;
		String[] result_column = new String[]{"_id" ,"prime_id","chap_id","upavacha_id","sam_text","mal_text","fav"};
		Cursor c =db.query(TABLE_NAME, result_column, where, null, null, null, null);
		if(c.moveToFirst())
			return new Shologam(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getString(4),c.getString(5) ,c.getString(6).equals("true"));
		return null;
	}
	public boolean updateFav(ContentValues contentValues,String where)
	{
		String where1 = "_id='"+where+"'";
		return db.update(TABLE_NAME, contentValues, where1, null)>0;
	}
	private class toDoCacheDbHelper extends SQLiteOpenHelper{
		private static final String DATABASE_CREATE = "create table if not exists "+TABLE_NAME+" " + "(_id integer,prime_id text,chap_id integer,upavacha_id integer,sam_text text,mal_text text,fav text);";
		public toDoCacheDbHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
			System.out.println("table created");
			
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			System.out.println("table created");
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
			System.out.println("BD UPDATEED");
			onCreate(db);
			
		}
		
	}
}
