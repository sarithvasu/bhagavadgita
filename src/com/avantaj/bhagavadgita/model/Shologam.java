package com.avantaj.bhagavadgita.model;

public class Shologam {
	private int id;
	private String quote_id;
	private int chap_id;
	private int upavcha;
	private String sam_text;
	private String mal_text;
	private boolean favorite;
	public Shologam(int id, String quote_id, int chap_id, int upavcha,
			String sam_text, String mal_text, boolean favorite) {
		super();
		this.id = id;
		this.quote_id = quote_id;
		this.chap_id = chap_id;
		this.upavcha = upavcha;
		this.sam_text = sam_text;
		this.mal_text = mal_text;
		this.favorite = favorite;
	}
	public int getId() {
		return id;
	}
	public String getQuote_id() {
		return quote_id;
	}
	public int getChap_id() {
		return chap_id;
	}
	public int getUpavcha() {
		return upavcha;
	}
	public String getSam_text() {
		return sam_text;
	}
	public String getMal_text() {
		return mal_text;
	}
	public boolean isFavorite() {
		return favorite;
	}
	
}
